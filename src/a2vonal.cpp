#include <GL/glut.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <cstdlib> // for rand()
#include <ctime>  // for random stuffs

#define PI 3.141592653589793

GLint keyStates[256]; // 0 nincs leütve 1= leütve

typedef struct point2d { GLdouble x, y; } POINT2D;

typedef struct kor {
    POINT2D midpoint;
    POINT2D iranyvektor;
    GLdouble r;
    bool felso;
    bool jobb;
}KOR;
std::vector<KOR> korok2;

// Ezek kellenek BASZDMEG!!!!!
POINT2D horizontal1 = {0, 300};
POINT2D horizontal2 = {800, 300};

POINT2D vertical1 = {400, 0};
POINT2D vertical2 = {400, 600};

// GLfloat x00 = 0, y00 = 300, x11 = 800, y11 = 300; // itt vannak a koordináták
GLdouble delta = 0.1;
GLdouble sikSzelessegf = 800.0;
GLdouble sikMagassagf = 600.0;
GLint sikSzelessegi = 800;
GLint sikMagassagi = 600;

// körös változók default circle size
GLdouble sugar = 30.0;

void init (void)
{
    glClearColor (1.0, 1.0, 1.0, 0.0);

    glMatrixMode (GL_PROJECTION);
    gluOrtho2D (0.0, sikSzelessegf, 0.0, sikMagassagf);
    glEnable( GL_POINT_SMOOTH );
    //glEnable(GL_LINE_STIPPLE);
    glPointSize( 4.0 ); // 10
    glLineWidth( 5.0 ); // 5.0
    glLineStipple(1, 0xFF00); // 1, 0xFF00
}
void circle(POINT2D O, GLdouble r) {

	glBegin(GL_LINE_LOOP);
	for (GLdouble t = 0; t <= 2 * PI; t += 0.01)
		  glVertex2d(O.x + r * cos(t), O.y + r * sin(t));
	glEnd();

}

// a distance itt pont és egyenes távolsága
double distance(int Ax, int Ay, int Bx, int By, int Px, int Py)   {
    double a, b, c, d;
    a = (double) (Bx - Ax);
    b = (double) (By - Ay);
    c = a * (By - Py) - b * (Bx - Px) ;
    return c / sqrt(a * a + b * b);
}

GLdouble distance2(GLdouble Ax, GLdouble Ay, GLdouble Bx, GLdouble By, GLdouble Px, GLdouble Py)   {
    double a, b, c;
    a =  (Bx - Ax);
    b =  (By - Ay);
    c = a * (By - Py) - b * (Bx - Px) ;
    return c / sqrt(a * a + b * b);
}

void keyPressed (unsigned char key, int x, int y) {
    keyStates[key] = 1;
}

void keyUp (unsigned char key, int x, int y) {
    keyStates[key] = 0;
}

// at a tükörvektor atx a tükörvektor x koordinátája ugyanígy y-nal
// a v a szóban forgó egyenes (amit vektorként vizsgálunk) normálvektora

/* eredeti fostos
void pattanFerdeVonalakrol ( KOR &inputOutputotVector, POINT2D egyenesPont1, POINT2D egyenesPont2 )    {

    POINT2D egyenesVektor;
    egyenesVektor.x = egyenesPont1.x -  egyenesPont2.x;
    egyenesVektor.y = egyenesPont1.y -  egyenesPont2.y;
    POINT2D normalVektor;
    normalVektor.x = egyenesVektor.y;
    normalVektor.y = egyenesVektor.x;
    normalVektor.x = normalVektor.x * (-1.0);
    //atx= 2 * (ax * vx + ay*vy) / (vx*vx + vy*vy) *vx -ax
    inputOutputotVector.iranyvektor.x = 2 * (( egyenesPont1.x * normalVektor.x + egyenesPont1.y * normalVektor.y ) / ( normalVektor.x *  normalVektor.x +  normalVektor.y * normalVektor.y )) * normalVektor.x - egyenesPont1.x;

    //aty= 2 * (ax *vx + ay*vy) / (vx*vx + vy*vy) *vy -ay
    inputOutputotVector.iranyvektor.y = 2 * (( egyenesPont1.x * normalVektor.x + egyenesPont1.y * normalVektor.y ) / ( normalVektor.x *  normalVektor.x +  normalVektor.y * normalVektor.y )) * normalVektor.y - egyenesPont1.y;
    std::cerr << "egyenesVektor.x: " << egyenesVektor.x << "\n";
    std::cerr << "egyenesVektor.y: " << egyenesVektor.y << "\n";
    std::cerr << "inputOutputotVector.iranyvektor.x: "  << inputOutputotVector.iranyvektor.x <<"\n";
    std::cerr << "inputOutputotVector.iranyvektor.y: "  << inputOutputotVector.iranyvektor.y << "\n";
    std::cerr << "=======================================================================================";
}
*/

void pattanFerdeVonalakrol ( KOR &inputOutputotVector, POINT2D egyenesPont1, POINT2D egyenesPont2 )    {

    //debugging
    /*
    std::cerr << "eredeti_inputOutputotVector.iranyvektor.x: "  << inputOutputotVector.iranyvektor.x <<"\n";
    std::cerr << "eredeti_inputOutputotVector.iranyvektor.y: "  << inputOutputotVector.iranyvektor.y << "\n";
    */

    POINT2D egyenesVektor;
    egyenesVektor.x = egyenesPont1.x -  egyenesPont2.x;
    egyenesVektor.y = egyenesPont1.y -  egyenesPont2.y;
    POINT2D normalVektor;
    normalVektor.x = egyenesVektor.y;
    normalVektor.y = egyenesVektor.x;
    normalVektor.x = normalVektor.x * (-1.0);

    POINT2D temporarySpeedVector;
    temporarySpeedVector.x =  inputOutputotVector.iranyvektor.x;
    temporarySpeedVector.y =  inputOutputotVector.iranyvektor.y;

    //atx= 2 * (ax * vx + ay*vy) / (vx*vx + vy*vy) *vx -ax
    inputOutputotVector.iranyvektor.x = 2 * (( temporarySpeedVector.x * normalVektor.x + temporarySpeedVector.y * normalVektor.y ) / ( normalVektor.x *  normalVektor.x +  normalVektor.y * normalVektor.y )) * normalVektor.x - temporarySpeedVector.x;

    //aty= 2 * (ax *vx + ay*vy) / (vx*vx + vy*vy) *vy -ay
    inputOutputotVector.iranyvektor.y = 2 * (( temporarySpeedVector.x * normalVektor.x + temporarySpeedVector.y * normalVektor.y ) / ( normalVektor.x *  normalVektor.x +  normalVektor.y * normalVektor.y )) * normalVektor.y - temporarySpeedVector.y;
    inputOutputotVector.iranyvektor.x = inputOutputotVector.iranyvektor.x * -1;
    inputOutputotVector.iranyvektor.y = inputOutputotVector.iranyvektor.y * -1;

    //debugging
    /*
    std::cerr << "inputOutputotVector.";
    if(inputOutputotVector.felso && inputOutputotVector.jobb)  {
        std::cerr << "felso jobb\n";
    }
    if(inputOutputotVector.felso && !inputOutputotVector.jobb)  {
        std::cerr << "felso bal\n";
    }
    if(!inputOutputotVector.felso && inputOutputotVector.jobb)  {
        std::cerr << "also jobb\n";
    }
    if(!inputOutputotVector.felso && !inputOutputotVector.jobb)  {
        std::cerr << "also jobb\n";
    }
    std::cerr << "egyenesVektor.x: " << egyenesVektor.x << "\n";
    std::cerr << "egyenesVektor.y: " << egyenesVektor.y << "\n";
    std::cerr << "inputOutputotVector.iranyvektor.x: "  << inputOutputotVector.iranyvektor.x <<"\n";
    std::cerr << "inputOutputotVector.iranyvektor.y: "  << inputOutputotVector.iranyvektor.y << "\n";
    std::cerr << "=======================================================================================\n";
    */

}

void pattanJobbrolBalrol ( KOR &input )   {
    input.iranyvektor.x = input.iranyvektor.x * (-1.0);
    input.midpoint.x = input.iranyvektor.x + input.midpoint.x;
}

void pattanFelulrolLentrol ( KOR &input )   {
    input.iranyvektor.y = input.iranyvektor.y * (-1.0);
    input.midpoint.y = input.iranyvektor.y + input.midpoint.y;
}

void koroketMozgat(int n)  {
    int nTreshold = 4;
    for(int i = 0; i < korok2.size(); i++)  {
        //felso sík
        GLdouble dTemp = 0.0;
        if( korok2[i].felso == true) {
            //bal felső sík
            if( korok2[i].jobb == false ) {
                // baloldalt ütközik
                if( 1.4 >= std::abs( (dTemp = distance2( 0.0, 0.0, 0.0, sikMagassagf,  korok2[i].midpoint.x,  korok2[i].midpoint.y) - korok2[i].r ) )  )  {
                    //std::cerr << "tavolsag baloldali utkozes elott: " << dTemp << "\n";
                    //std::cerr << "=======================================================================================\n";
                    pattanJobbrolBalrol ( korok2[i] );
                    /*
                    korok2[i].iranyvektor.x = korok2[i].iranyvektor.x * (-1.0);
                    korok2[i].midpoint.x = korok2[i].iranyvektor.x + korok2[i].midpoint.x;
                    */
                }
                // fent ütközik
                else if ( 1.4 >= std::abs(  (dTemp =  distance2( 0.0, sikMagassagf, sikSzelessegf, sikMagassagf,  korok2[i].midpoint.x,  korok2[i].midpoint.y) - korok2[i].r ) ) ) {
                    //std::cerr << "tavolsag fenti utkozes elott: " << dTemp << "\n";
                    //std::cerr << "=======================================================================================\n";
                    pattanFelulrolLentrol ( korok2[i] );
                }
                // jobboldalt ütközik
                else if (1.4 >= std::abs(  (dTemp = distance2(  vertical1.x,  vertical1.y,  vertical2.x,  vertical2.y,  korok2[i].midpoint.x,  korok2[i].midpoint.y) + korok2[i].r  ) ) ) {
                    //std::cerr << "tavolsag jobboldali utkozes elott: " << dTemp << "\n";
                    //std::cerr << "=======================================================================================\n";
                    pattanFerdeVonalakrol ( korok2[i], vertical1 , vertical2 );
                    korok2[i].midpoint.x = korok2[i].iranyvektor.x + korok2[i].midpoint.x;
                    korok2[i].midpoint.y = korok2[i].iranyvektor.y + korok2[i].midpoint.y;
                }
                // lenn ütközik
                else if ( 1.4 >= std::abs(  (dTemp = distance2(  horizontal1.x,  horizontal1.y,  horizontal2.x,  horizontal2.y,  korok2[i].midpoint.x,  korok2[i].midpoint.y) + korok2[i].r ) ) )  {
                    //std::cerr << "tavolsag lenti utkozes elott: " << dTemp << "\n";
                    //std::cerr << "=======================================================================================\n";
                    pattanFerdeVonalakrol ( korok2[i], horizontal1 , horizontal2 );
                    korok2[i].midpoint.x = korok2[i].iranyvektor.x + korok2[i].midpoint.x;
                    korok2[i].midpoint.y = korok2[i].iranyvektor.y + korok2[i].midpoint.y;
                }
                else    {
                    korok2[i].midpoint.x = korok2[i].iranyvektor.x + korok2[i].midpoint.x;
                    korok2[i].midpoint.y = korok2[i].iranyvektor.y + korok2[i].midpoint.y;
                }

            }
            //jobb felső sík
            else    {
                // baloldalt ütközik
                if( 1.4 >= std::abs( distance2(  vertical1.x,  vertical1.y,  vertical2.x,  vertical2.y,  korok2[i].midpoint.x,  korok2[i].midpoint.y) - korok2[i].r ) )  {
                    pattanFerdeVonalakrol ( korok2[i], vertical1 , vertical2 );
                    korok2[i].midpoint.x = korok2[i].iranyvektor.x + korok2[i].midpoint.x;
                    korok2[i].midpoint.y = korok2[i].iranyvektor.y + korok2[i].midpoint.y;
                }
                // fent ütközik
                else if (1.4 >= std::abs( distance2( 0.0, sikMagassagf, sikSzelessegf, sikMagassagf,  korok2[i].midpoint.x, korok2[i].midpoint.y) - korok2[i].r ) ) {
                    pattanFelulrolLentrol ( korok2[i] );
                }
                // jobboldalt ütközik
                else if (1.4 >= std::abs( distance2( sikSzelessegf, 0.0, sikSzelessegf, sikMagassagf, korok2[i].midpoint.x, korok2[i].midpoint.y) + korok2[i].r   ) ) {
                    pattanJobbrolBalrol ( korok2[i] );
                }
                // lenn ütközik
                else if ( 1.4 >= std::abs( distance2( horizontal1.x, horizontal1.y, horizontal2.x, horizontal2.y, korok2[i].midpoint.x, korok2[i].midpoint.y) + korok2[i].r ) ) {
                    pattanFerdeVonalakrol ( korok2[i], horizontal1 , horizontal2 );
                    korok2[i].midpoint.x = korok2[i].iranyvektor.x + korok2[i].midpoint.x;
                    korok2[i].midpoint.y = korok2[i].iranyvektor.y + korok2[i].midpoint.y;
                }
                else    {
                    korok2[i].midpoint.x = korok2[i].iranyvektor.x + korok2[i].midpoint.x;
                    korok2[i].midpoint.y = korok2[i].iranyvektor.y + korok2[i].midpoint.y;
                }

            }
        }
        //also sík
        else  {
            //bal alsó sík

            if( korok2[i].jobb == false ) {
                // baloldalt ütközik
                if(1.4 >= std::abs( distance2( 0.0, 0.0, 0.0, sikMagassagi,  korok2[i].midpoint.x,  korok2[i].midpoint.y) - korok2[i].r ) ) {
                    pattanJobbrolBalrol ( korok2[i] );
                }
                // fent ütközik
                else if (1.4 >= std::abs(  distance2( horizontal1.x,  horizontal1.y,  horizontal2.x,  horizontal2.y,  korok2[i].midpoint.x, korok2[i].midpoint.y) - korok2[i].r )) {
                    pattanFerdeVonalakrol ( korok2[i], horizontal1 , horizontal2 );
                    korok2[i].midpoint.x = korok2[i].iranyvektor.x + korok2[i].midpoint.x;
                    korok2[i].midpoint.y = korok2[i].iranyvektor.y + korok2[i].midpoint.y;
                }
                // jobboldalt ütközik
                else if ( 1.4 >= std::abs(  distance2( vertical1.x,  vertical1.y, vertical2.x, vertical2.y, korok2[i].midpoint.x, korok2[i].midpoint.y) + korok2[i].r   ) ) {
                    pattanFerdeVonalakrol ( korok2[i], vertical1 , vertical2 );
                    korok2[i].midpoint.x = korok2[i].iranyvektor.x + korok2[i].midpoint.x;
                    korok2[i].midpoint.y = korok2[i].iranyvektor.y + korok2[i].midpoint.y;
                }
                // lenn ütközik
                else if (1.4 >= std::abs(  distance2( 0.0, 0.0, sikSzelessegf, 0.0, korok2[i].midpoint.x, korok2[i].midpoint.y) + korok2[i].r ) )  {
                    pattanFelulrolLentrol ( korok2[i] );
                }
                else    {
                    korok2[i].midpoint.x = korok2[i].iranyvektor.x + korok2[i].midpoint.x;
                    korok2[i].midpoint.y = korok2[i].iranyvektor.y + korok2[i].midpoint.y;
                }
            }
            //jobb alsó sík
            else    {
                // baloldalt ütközik
                if( 1.4 >= std::abs( distance2(  vertical1.x,  vertical1.y,  vertical2.x,  vertical2.y,  korok2[i].midpoint.x,  korok2[i].midpoint.y) - korok2[i].r ) )  {
                    pattanFerdeVonalakrol ( korok2[i], vertical1 , vertical2 );
                    korok2[i].midpoint.x = korok2[i].iranyvektor.x + korok2[i].midpoint.x;
                    korok2[i].midpoint.y = korok2[i].iranyvektor.y + korok2[i].midpoint.y;
                }
                // fent ütközik
                else if ( 1.4 >= std::abs( distance2(  horizontal1.x,  horizontal1.y,  horizontal2.x,  horizontal2.y,  korok2[i].midpoint.x,  korok2[i].midpoint.y) - korok2[i].r ) ) {
                    pattanFerdeVonalakrol ( korok2[i], horizontal1 , horizontal2 );
                    korok2[i].midpoint.x = korok2[i].iranyvektor.x + korok2[i].midpoint.x;
                    korok2[i].midpoint.y = korok2[i].iranyvektor.y + korok2[i].midpoint.y;
                }
                // jobboldalt ütközik
                else if ( 1.4 >= std::abs( distance2( sikSzelessegf, 0.0, sikSzelessegf, sikMagassagf,  korok2[i].midpoint.x,  korok2[i].midpoint.y) + korok2[i].r  ) ) {
                    pattanJobbrolBalrol ( korok2[i] );
                }
                // lenn ütközik
                else if ( 1.4 >= std::abs( distance2( 0.0, 0.0, sikSzelessegf, 0.0,  korok2[i].midpoint.x,  korok2[i].midpoint.y) + korok2[i].r ) )  {
                    pattanFelulrolLentrol ( korok2[i] );
                }
                else    {
                    korok2[i].midpoint.x = korok2[i].iranyvektor.x + korok2[i].midpoint.x;
                    korok2[i].midpoint.y = korok2[i].iranyvektor.y + korok2[i].midpoint.y;
                }
            }
        }
    }
    glutPostRedisplay( );

    glutTimerFunc( 5, koroketMozgat, 0 );
    // end of koroket mozgat function
}

void keyOperations ( ) {
    if (keyStates['a'])   {
      	//x00-=delta;
        vertical2.x = vertical2.x - delta;
        vertical1.x = vertical1.x + delta;
    }
    if (keyStates['d']) {
        vertical2.x = vertical2.x + delta;
        vertical1.x = vertical1.x - delta;
    }
    if (keyStates['s']) {
        horizontal2.y = horizontal2.y - delta;
        horizontal1.y = horizontal1.y + delta;
    }
    if (keyStates['w']) {
        horizontal2.y = horizontal2.y + delta;
        horizontal1.y = horizontal1.y - delta;
    }
    // nem switch mert tobb agon van
    glutPostRedisplay( );
}

// rajzolofuggvenyben van olyan fuggveny ami a kirajzolashoz szukseges
void draw (void)
{
    keyOperations();      //
    glClear (GL_COLOR_BUFFER_BIT);
    glColor3f (0.0, 0.4, 0.2);
    glBegin (GL_LINES);
        glVertex2f (horizontal1.x, horizontal1.y);
        glVertex2f (horizontal2.x, horizontal2.y);
        glVertex2f (vertical1.x, vertical1.y);
        glVertex2f (vertical2.x, vertical2.y);
    glEnd ( );
    //pontok kibaszása
    for(int j = 0; j < sikMagassagi ; j = j + 20) {
        for(int i = 0; i < sikSzelessegi ; i = i + 20) {
            //piros, zöld, kék, sárga
            // piros helyett pink 255 0 222
            // zöld 0, 255, 0
            glBegin( GL_POINTS );
                // bal felső
                if(     0 > distance( (int) vertical1.x, (int) vertical1.y, (int) vertical2.x, (int) vertical2.y, i, j )
                    &&
                        0 > distance( (int) horizontal1.x, (int) horizontal1.y, (int) horizontal2.x, (int) horizontal2.y, i, j )
                   )  {
                      //pink
                      glColor3ub(255, 0, 222);
                }
                // jobb felső
                else if(     0 < distance( (int) vertical1.x, (int) vertical1.y, (int) vertical2.x, (int) vertical2.y, i, j )
                            &&
                             0 > distance( (int) horizontal1.x, (int) horizontal1.y, (int) horizontal2.x, (int) horizontal2.y, i, j )
                       )  {
                      //ződ fű
                      glColor3ub(0, 255, 0);
                }
                // jobb alsó
                else if(     0 < distance( (int) vertical1.x, (int) vertical1.y, (int) vertical2.x, (int) vertical2.y, i, j )
                            &&
                             0 < distance( (int) horizontal1.x, (int) horizontal1.y, (int) horizontal2.x, (int) horizontal2.y, i, j )
                       )  {
                      //kek lol
                      glColor3ub(0, 0, 255);
                }
                // bal alsó
                else if(     0 > distance( (int) vertical1.x, (int) vertical1.y, (int) vertical2.x, (int) vertical2.y, i, j )
                            &&
                             0 < distance( (int) horizontal1.x, (int) horizontal1.y, (int) horizontal2.x, (int) horizontal2.y, i, j )
                       )  {
                      // oranzs
                      glColor3ub(255, 128, 0);
                }
                glVertex2f( (float) i, (float)j );
            glEnd();

            // debug
            // ha kisebb a v pont x koordinátája kisebb? mint a vonal x koordinátája, akkor a függvény eredménye negatív lesz
            /*
            std::cerr << "a pont x,y: " << i << ", " << j << "\n";
            std::cerr << "es a tavolsaga: " << distance( (int) vertical1.x, (int) vertical1.y, (int) vertical2.x, (int) vertical2.y, i, j ) <<"\n";
            std::cerr << "===================================================\n";
            */

            // itt hogyha a pont y koordinátája kisebb mint a vonal y koordinátája-ja, akkor a függvény eredménye pozitív
            /*
            std::cerr << "a pont x,y: " << i << ", " << j << "\n";
            std::cerr << "es a tavolsaga: " << distance( (int) horizontal1.x, (int) horizontal1.y, (int) horizontal2.x, (int) horizontal2.y, i, j ) <<"\n";
            std::cerr << "===================================================\n";
            */
        }
    }
    if(korok2.size() > 0)  {
        for(int iter = 0; iter < korok2.size(); ++iter)  {
            circle(korok2[iter].midpoint, korok2[iter].r);
            // debug
            //std::cerr << "kor rajzolva lett:" << iter <<"\n";
        }
    }
    glutSwapBuffers( );
    // end of draw
}

GLfloat dist2( POINT2D P1, POINT2D P2 ) {
    GLfloat t1 = P1.x - P2.x;
    GLfloat t2 = P1.y - P2.y;
    return t1 * t1 + t2 * t2;
}

POINT2D initPoint2D( GLfloat x, GLfloat y ) {
    POINT2D P;
    P.x = x;
    P.y = y;
    return P;
}

GLint getActivePoint1( POINT2D inputPoint, GLint size, GLint sens, GLint x, GLint y ) {
    GLint i, s = sens * sens;
    POINT2D localPoint = initPoint2D( x, y );

    if ( dist2( inputPoint, localPoint ) < s )
        return 1;
    else
        return -1;
}

void processMouse( GLint button, GLint action, GLint xMouse , GLint yMouse )  {
    KOR Korom;
    Korom.midpoint.x = xMouse;
    Korom.midpoint.y = sikMagassagf - yMouse;
    Korom.r = sugar;
    Korom.iranyvektor.x = ((static_cast <double> (rand()) / static_cast <double> (RAND_MAX )) - 0.5) * 1.6; //2.0 val lesz az irány 0.0 és 1.0 között
    Korom.iranyvektor.y = ((static_cast <double> (rand()) / static_cast <double> (RAND_MAX )) - 0.5) * 1.6;
    if(  0 > distance( (int) vertical1.x, (int) vertical1.y, (int) vertical2.x, (int) vertical2.y, xMouse, sikMagassagi - yMouse ) )  {
          Korom.jobb = false;
    }
    else  {
        Korom.jobb = true;
    }
    if ( 0 > distance( (int) horizontal1.x, (int) horizontal1.y, (int) horizontal2.x, (int) horizontal2.y, xMouse, sikMagassagi - yMouse ) )   {
        Korom.felso = true;
    }
    else  {
        Korom.felso = false;
    }

    if ( button == GLUT_LEFT_BUTTON && action == GLUT_DOWN )  {
        korok2.push_back(Korom); //ez a vektorbapakolos
        glutPostRedisplay( );
    }
    if ( button == GLUT_RIGHT_BUTTON && action == GLUT_DOWN )  {
        for(int iii = 0; iii < korok2.size(); ++iii)  {
            if(korok2[iii].r * korok2[iii].r > dist2(korok2[iii].midpoint, initPoint2D(xMouse, sikMagassagi - yMouse)))  {
                korok2.erase(korok2.begin() + iii);
            }
        }
        /*
        korok2.push_back(Korom); //ez a vektorbapakolos
        glutPostRedisplay( );
        if ( ( i = getActivePoint1( points, 4, 8, xMouse, winHeight - yMouse ) ) != -1 )
            dragged = i;
        */
    }
}

int main (int argc, char** argv)
{
    srand (static_cast <unsigned> (time(0)));
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowPosition (50, 100);
    glutInitWindowSize (sikSzelessegi, sikMagassagi);
    glutCreateWindow ("elsoHF");
    init ( );
    glutDisplayFunc (draw);
    glutKeyboardFunc(keyPressed); // ha leütnek egy billentyut belerakja a tombbe
    glutKeyboardUpFunc(keyUp);    // ezt meghívja ha felengedjük a billentyut
    glutMouseFunc( processMouse );
    glutTimerFunc( 5, koroketMozgat, 0 );
    glutMainLoop ( );
    return 0;
}
